

/* jshint node: true, devel: true */
'use strict';

const
    bodyParser = require('body-parser'),
    config = require('config'),
    crypto = require('crypto'),
    express = require('express'),
    https = require('https'),
    request = require('request'),
    passport = require('passport'),
    session = require('express-session'),
        URL = require('url'),
        URLSearchParam = require('url');

require('./passport')(passport); // pass passport for configuration
const scbNodeParser = require('scb-node-parser');
var Message = require('scb-node-parser').Message;
var sender = require('./amqp-sender');
var confamqp = require('./conf/amqp-endpoint.conf');

var Message = require('scb-node-parser/message');
var HashMap = require('hashmap');

var requestPromise = require('request-promise');

var app = express();
app.set('view engine', 'ejs');
app.use(bodyParser.json({
    verify: verifyRequestSignature
}));
app.use(bodyParser.urlencoded({
    extended: false
})) // parse application/x-www-form-urlencoded
app.use(express.static('public'));
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
})); // session secret
app.use(passport.initialize());
app.use(passport.session());

/*
 * Be sure to setup your config values before running this code. You can
 * set them using environment variables or modifying the config file in /config.
 *
 */

// App Secret can be retrieved from the App Dashboard
const APP_SECRET = (process.env.MESSENGER_APP_SECRET) ?
    process.env.MESSENGER_APP_SECRET :
    config.get('appSecret');

// Arbitrary value used to validate a webhook
const VALIDATION_TOKEN = (process.env.MESSENGER_VALIDATION_TOKEN) ?
    (process.env.MESSENGER_VALIDATION_TOKEN) :
    config.get('validationToken');

// Generate a page access token for your page from the App Dashboard
const PAGE_ACCESS_TOKEN = (process.env.MESSENGER_PAGE_ACCESS_TOKEN) ?
    (process.env.MESSENGER_PAGE_ACCESS_TOKEN) :
    config.get('pageAccessToken');

// URL where the app is running (include protocol). Used to point to scripts and
// assets located at this address.
const SERVER_URL = (process.env.SERVER_URL) ?
    (process.env.SERVER_URL) :
    config.get('serverURL');

if (!(APP_SECRET && VALIDATION_TOKEN && PAGE_ACCESS_TOKEN && SERVER_URL)) {
    console.error("Missing config values");
    process.exit(1);
}


app.get('/checkbox', (req, res) => {
    res.render('sendToMessenger', {});
});

/*
 * Use your own validation token. Check that the token used in the Webhook
 * setup is the same token used here.
 *
 */
app.get('/webhook', (req, res) => {
    res.status(200).send(req.query['hub.challenge']);
    if (req.query['hub.mode'] === 'subscribe' &&
        req.query['hub.verify_token'] === VALIDATION_TOKEN) {
        console.log("Validating webhook");
        res.status(200).send(req.query['hub.challenge']);
    } else {
        console.error("Failed validation. Make sure the validation tokens match.");
        res.sendStatus(403);
    }
});




/*
 * All callbacks for Messenger are POST-ed. They will be sent to the same
 * webhook. Be sure to subscribe your app to your page to receive callbacks
 * for your page.
 * https://developers.facebook.com/docs/messenger-platform/product-overview/setup#subscribe_app
 *
 */

app.post('/webhook', (req, res) => {
    var data = req.body;

    // Make sure this is a page subscription
    if (data.object == 'page') {
        // Iterate over each entry
        // There may be multiple if batched
        data.entry.forEach((pageEntry) => {
            var pageID = pageEntry.id;
            var timeOfEvent = pageEntry.time;

            // Iterate over each messaging event
            pageEntry.messaging.forEach((messagingEvent) => {
                if (messagingEvent.optin) {
                    receivedAuthentication(messagingEvent);
                } else if (messagingEvent.message) {
                    receivedMessage(messagingEvent);
                } else if (messagingEvent.delivery) {
                    receivedDeliveryConfirmation(messagingEvent);
                } else if (messagingEvent.postback) {
                    receivedPostback(messagingEvent);
                } else if (messagingEvent.read) {
                    receivedMessageRead(messagingEvent);
                } else if (messagingEvent.account_linking) {
                    receivedAccountLink(messagingEvent);
                } else {
                    console.log("Webhook received unknown messagingEvent: ", messagingEvent);
                }
            });
        });

        // Assume all went well.
        //
        // You must send back a 200, within 20 seconds, to let us know you've
        // successfully received the callback. Otherwise, the request will time out.
        res.sendStatus(200);
    }
});

/*app.post('/linkaccount', function(req, res) {
    console.log('linking accounts...');
    var url = require('url');
    var url_parts = url.parse(request.url, true);
    var query = url_parts.query;
    console.log('linking accounts : ' + query.uname + ' and ');

    console.log(req);
    res.sendStatus(202);
});*/

/*app.post('/login', passport.authenticate('local', {
    //successRedirect: '/logged', // redirect to the secure profile section
    failureRedirect: '/authenticate', // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
}), function(req, res) {
    res.redirect('/');
});*/

/*app.post('/authenticate',
    passport.authenticate('local', {
        failureRedirect: '/authenticate',
        successRedirect: '/logged'
    })
);*/

app.post('/authenticate', (req, res, next) => {

    passport.authenticate('local', (err, user, info) => {
        if (err) {
            return next(err);
        }
        // Redirect if it fails
        if (!user) {
            return res.redirect('/authenticate');
        }
        req.logIn(user, (err) => {
            if (err) {
                return next(err);
            }
            //login success
            //get authcode and business user entity to match them later
            //when Account Linking Callback is received:
            // https://developers.facebook.com/docs/messenger-platform/webhook-reference/account-linking
            // in function receivedAccountLink(event)
            const url = new URL(req.body.redirect);
            const params = new URLSearchParams(url.search);
            console.log("authorization_code " + params.get('authorization_code'));
            app.get('cache').set(params.get('authorization_code'), user.username);
            // Redirect
            return res.redirect(req.body.redirect);
        });
    })(req, res, next);
});

/*
 * This path is used for account linking. The account linking call-to-action
 * (sendAccountLinking) is pointed to this URL.
 *
 */
app.get('/authenticate', (req, res) => {
    var accountLinkingToken = req.query.account_linking_token;
    var redirectURI = req.query.redirect_uri;

    // Authorization Code should be generated per user by the developer. This will
    // be passed to the Account Linking callback and used to match
    // the business user entity to the page-scoped ID (PSID) of the sender
    var min = 1;
    var max = Number.MAX_SAFE_INTEGER;
    const authCode = (Math.floor(Math.random() * (max - min + 1)) + min).toString();

    // Redirect users to this URI on successful login
    var redirectURISuccess = redirectURI + "&authorization_code=" + authCode;
    res.render('login', {
        accountLinkingToken: accountLinkingToken,
        redirectURI: redirectURI,
        redirectURISuccess: redirectURISuccess
    });
});

/*
 * Verify that the callback came from Facebook. Using the App Secret from
 * the App Dashboard, we can verify the signature that is sent with each
 * callback in the x-hub-signature field, located in the header.
 *
 * https://developers.facebook.com/docs/graph-api/webhooks#setup
 *
 */
function verifyRequestSignature(req, res, buf) {
    var signature = req.headers["x-hub-signature"];

    if (!signature) {
        // For testing, let's log an error. In production, you should throw an
        // error.
        console.error("Couldn't validate the signature.");
    } else {
        var elements = signature.split('=');
        var method = elements[0];
        var signatureHash = elements[1];

        var expectedHash = crypto.createHmac('sha1', APP_SECRET)
            .update(buf)
            .digest('hex');

        if (signatureHash != expectedHash) {
            throw new Error("Couldn't validate the request signature.");
        }
    }
}

/*
 * Authorization Event
 *
 * The value for 'optin.ref' is defined in the entry point. For the "Send to
 * Messenger" plugin, it is the 'data-ref' field. Read more at
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/authentication
 *
 */
function receivedAuthentication(event) {
    console.log("received authentication: " + JSON.stringify(event));
    if (event.optin != 'undefined') {
        console.log("sending message using user ref: " + event.optin.user_ref +
            " for logged user " + event.optin.ref);

        var messageData = {
            recipient: {
                user_ref: event.optin.user_ref
            },
            message: {
                text: "Welcome!"
            }
        };

        callSendAPICheckbox(messageData, event.optin.ref);
    } else {

        var senderID = event.sender.id;
        var recipientID = event.recipient.id;
        var timeOfAuth = event.timestamp;

        // The 'ref' field is set in the 'Send to Messenger' plugin, in the 'data-ref'
        // The developer can set this to an arbitrary value to associate the
        // authentication callback with the 'Send to Messenger' click event. This is
        // a way to do account linking when the user clicks the 'Send to Messenger'
        // plugin.
        var passThroughParam = event.optin.ref;

        console.log("Received authentication for user %d and page %d with pass " +
            "through param '%s' at %d", senderID, recipientID, passThroughParam,
            timeOfAuth);

        // When an authentication is received, we'll send a message back to the sender
        // to let them know it was successful.
        sendTextMessage(senderID, "Authentication successful");
    }
}

/*
 * Message Event
 *
 * This event is called when a message is sent to your page. The 'message'
 * object format can vary depending on the kind of message that was received.
 * Read more at https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-received
 *
 * For this example, we're going to echo any text that we get. If we get some
 * special keywords ('button', 'generic', 'receipt'), then we'll send back
 * examples of those bubbles to illustrate the special message bubbles we've
 * created. If we receive a message with an attachment (image, video, audio),
 * then we'll simply confirm that we've received the attachment.
 *
 */
function receivedMessage(event) {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;
    var timeOfMessage = event.timestamp;
    var message = event.message;

    console.log("Received message for user %d and page %d at %d with message:",
        senderID, recipientID, timeOfMessage);
    console.log(JSON.stringify(message));

    var isEcho = message.is_echo;
    var messageId = message.mid;
    var appId = message.app_id;
    var metadata = message.metadata;

    // You may get a text or attachment but not both
    var messageText = message.text;
    var messageAttachments = message.attachments;
    var quickReply = message.quick_reply;

    if (isEcho) {
        // Just logging message echoes to console
        console.log("Received echo for message %s and app %d with metadata %s",
            messageId, appId, metadata);
        return;
    } else if (quickReply) {

        var quickReplyPayload = quickReply.payload;
        console.log("Quick reply for message %s with payload %s",
            messageId, quickReplyPayload);

        switch (quickReplyPayload) {
            case 'USERS':
                //sendTextMessage(senderID, 'to whom?');
                var message = new Message();
                message._from = {
                    name:  event.sender.name,
                    uniqueName: senderID
                };
                message._message = "ls";
                message._type = "COMMAND";
                message._persona = confamqp.exchange.name;
                sender.post(message);
                break;
            default:
                var message = new Message();
                message._from = {
                    name:  event.sender.name,
                    uniqueName: senderID
                };
                message._message = quickReplyPayload.toLowerCase();
                message._type = "COMMAND";
                message._persona = confamqp.exchange.name;
                sender.post(message);
            }
        return;
    }
    console.log('User state: ' + userState.get(senderID));
    console.log('UMessage: ' + messageText.toLowerCase());
    if(userState.get(senderID) != undefined) {
        if(messageText.toLowerCase() === "quit") {
            sendEndSeparator(senderID, 'You ended the conversation with ' + userState.get(senderID) + '.');
            userState.delete(senderID);
        } else {
            //sendTextMessage(senderID, 'Message sent to ' + userState.get(senderID) + '!');
            var message = new Message();
            message._from = {
                name:  event.sender.name,
                uniqueName: senderID
            };
            message._to = [
                userState.get(senderID)];
            message._message = messageText;
            message._type = 'TO';
            message._persona = confamqp.exchange.name;
            sender.post(message);
        }
    } else if (messageText) {

        // If we receive a text message, check to see if it matches any special
        // keywords and send back the corresponding example. Otherwise, just echo
        // the text we received.
        switch (messageText) {
            case 'image':
                sendImageMessage(senderID);
                break;

            case 'gif':
                sendGifMessage(senderID);
                break;

            case 'audio':
                sendAudioMessage(senderID);
                break;

            case 'video':
                sendVideoMessage(senderID);
                break;

            case 'file':
                sendFileMessage(senderID);
                break;

            case 'button':
                sendButtonMessage(senderID);
                break;

            case 'generic':
                sendGenericMessage(senderID);
                break;

            case 'receipt':
                sendReceiptMessage(senderID);
                break;

            case 'quick reply':
                sendQuickReply(senderID);
                break;
            case 'add menu':
                console.log('add menu');
                break 
            case 'read receipt':
                sendReadReceipt(senderID);
                break;

            case 'typing on':
                sendTypingOn(senderID);
                break;

            case 'typing off':
                sendTypingOff(senderID);
                break;

            case 'account linking':
                sendAccountLinking(senderID);
                break;

            default:
                /*var parsedMessage = scbNodeParser.getMessage(messageText);
                console.log('parsedMessage: ' + parsedMessage.getMessage());
                parsedMessage._from = {
                    name:  event.sender.name,
                    uniqueName: senderID
                };
                parsedMessage._persona = confamqp.exchange.name;
                sender.post(parsedMessage);*/
                console.log('receivedMessage.default');
                sendQuickReply(senderID);
        }
    } else if (messageAttachments) {
        sendTextMessage(senderID, "Message with attachment received");
    }
}

/** 
 * Messages from socialbus
 **/
function post(msg) {

    switch (msg._type) {
        case 'LS':
            sendUserList(msg._to.uniqueName, msg._message);
            break;
        case 'TO':
            console.log('Message from social bus ' + userState.get(msg._to.uniqueName));
            if(userState.get(msg._to.uniqueName) === undefined) {
                userState.set(msg._to.uniqueName, msg._from.name);
                console.log('userState set for ' + msg._to.uniqueName + ' set to ' + msg._from.uniqueName);

                var uname = msg._to.uniqueName;
                var umessage = msg._message;
                sendStartSeparator(msg._to.uniqueName, "This is a conversation with " +  
                msg._from.name + ". Write \"quit\" to end it.", 
                    function(uname, umessage)  {
                        sendTextMessage(msg._to.uniqueName, msg._message);
                    }
                );      
                
            } else {

                sendTextMessage(msg._to.uniqueName, msg._message);
            }
            break;
        case 'ABOUT':
            sendGenericMessage(msg._to.uniqueName, msg._message);
            break;
        case 'INFO':
            sendTextMessage(msg._to.uniqueName, msg._message);
            break;
        default:

    }
    /*var messageText = '';
    if(msg._from.uniqueName === 'socialbus')
        messageText = msg.getMessage();
    else
        messageText = 'From: ' + msg.getFrom().name + '. ' + msg.getSubject() + '\n Message: \n' + msg.getMessage();
    */
    
}


/*
 * Delivery Confirmation Event
 *
 * This event is sent to confirm the delivery of a message. Read more about
 * these fields at https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-delivered
 *
 */
function receivedDeliveryConfirmation(event) {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;
    var delivery = event.delivery;
    var messageIDs = delivery.mids;
    var watermark = delivery.watermark;
    var sequenceNumber = delivery.seq;

    if (messageIDs) {
        messageIDs.forEach((messageID) => {
            console.log("Received delivery confirmation for message ID: %s",
                messageID);
        });
    }

    console.log("All message before %d were delivered.", watermark);
}


/*
 * Postback Event
 *
 * This event is called when a postback is tapped on a Structured Message.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/postback-received
 *
 */
var userState = new HashMap();
function receivedPostback(event) {
    console.log('log');
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;
    var timeOfPostback = event.timestamp;

    // The 'payload' param is a developer-defined field which is set in a postback
    // button for Structured Messages.
    var payload = event.postback.payload;

    console.log("Received postback for user %d and page %d with payload '%s' " +
        "at %d", senderID, recipientID, payload, timeOfPostback);

    // When a postback is called, we'll send a message back to the sender to
    // let them know it was successful
    var jPayload = JSON.parse(payload);
    //sendTextMessage(senderID, "Postback called from action " + JSON.stringify(jPayload.action));
    switch(jPayload.action) {
        case 'TO':

            userState.set(senderID, jPayload.userID);

            sendStartSeparator(senderID, "This is a conversation with " +  
            jPayload.userID + ". Write \"quit\" to end it.");
            break;
        case 'MAIN_MENU':
            sendQuickReply(senderID);
            break;
        default:
    }
}

/*
 * Message Read Event
 *
 * This event is called when a previously-sent message has been read.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-read
 *
 */
function receivedMessageRead(event) {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;

    // All messages before watermark (a timestamp) or sequence have been seen.
    var watermark = event.read.watermark;
    var sequenceNumber = event.read.seq;

    console.log("Received message read event for watermark %d and sequence " +
        "number %d", watermark, sequenceNumber);
}

/*
 * Account Link Event
 *
 * This event is called when the Link Account or UnLink Account action has been
 * tapped.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/account-linking
 *
 */
function receivedAccountLink(event) {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;

    var status = event.account_linking.status;
    var authCode = event.account_linking.authorization_code;

    const userApplicationId = app.get('cache').get(authCode);
    console.log("Received account link event with for user %d with status %s " +
        "and auth code %s for userApplicationId %s", senderID, status, authCode, userApplicationId);

    //delete the key since we'll not need it anymore
    app.get('cache').del(authCode);
    addIdentity(senderID, userApplicationId);
    sendTextMessage(senderID,
        "Done! You've been linked with username \"" + userApplicationId + "\"");
}

/*
 * Send an image using the Send API.
 *
 */
function sendImageMessage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "image",
                payload: {
                    url: SERVER_URL + "/assets/rift.png"
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a Gif using the Send API.
 *
 */
function sendGifMessage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "image",
                payload: {
                    url: SERVER_URL + "/assets/instagram_logo.gif"
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send audio using the Send API.
 *
 */
function sendAudioMessage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "audio",
                payload: {
                    url: SERVER_URL + "/assets/sample.mp3"
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a video using the Send API.
 *
 */
function sendVideoMessage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "video",
                payload: {
                    url: SERVER_URL + "/assets/allofus480.mov"
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a file using the Send API.
 *
 */
function sendFileMessage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "file",
                payload: {
                    url: SERVER_URL + "/assets/test.txt"
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a text message using the Send API.
 *
 */
function sendTextMessage(recipientId, messageText) {
    console.log('recipientId : ' + recipientId);
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            text: messageText,
            metadata: "DEVELOPER_DEFINED_METADATA"
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a text message using the Send API.
 *
 */
function send(message) {
    console.log('recipientName : ' + message._to.name + ' recipientId : ' + message._to.uniqueName);
    var messageData = {
        recipient: {
            id: message._to.uniqueName
        },
        message: {
            text: message._message,
            metadata: "DEVELOPER_DEFINED_METADATA"
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a button message using the Send API.
 *
 */
function sendButtonMessage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "button",
                    text: "This is test text",
                    buttons: [{
                        type: "web_url",
                        url: "https://www.oculus.com/en-us/rift/",
                        title: "Open Web URL"
                    }, {
                        type: "postback",
                        title: "Trigger Postback",
                        payload: "DEVELOPER_DEFINED_PAYLOAD"
                    }, {
                        type: "phone_number",
                        title: "Call Phone Number",
                        payload: "+16505551234"
                    }]
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a Structured Message (Generic Message type) using the Send API.
 *
 */
function sendGenericMessage(recipientId, aboutMessage) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "generic",
                    elements: [{
                        title: "About me",
                        subtitle: aboutMessage.text,
                        item_url: "https://gitlab.inria.fr/usnb/universal-social-network-bus",
                        image_url: aboutMessage.picture,
                        buttons: [{
                            "title": "Main Menu",
                            "type": "postback",
                            "payload": JSON.stringify({
                                "action": "MAIN_MENU",
                                "userID": recipientId
                                })    
                        }, {
                            "type":"web_url",
                            "url": aboutMessage.url,
                            "title": "Visit Page",
                            "webview_height_ratio": "full"
                        }],
                    }]
                }
            }
        }
    };

    callSendAPI(messageData);
}

function sendStartSeparator(recipientId, message, callback) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "generic",
                    elements: [{
                        title: message,
                        //subtitle: aboutMessage.text,
                        //item_url: "https://gitlab.inria.fr/usnb/universal-social-network-bus",
                        image_url: "https://preview.ibb.co/jtM978/twitter_292988_1920.jpg",
                        /*buttons: [{
                            "title": "Main Menu",
                            "type": "postback",
                            "payload": JSON.stringify({
                                "action": "MAIN_MENU",
                                "userID": recipientId
                                })    
                        }]*/
                    }]
                }
            }
        }
    };

    requestPromise({
        uri: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {
            access_token: PAGE_ACCESS_TOKEN
        },
        method: 'POST',
        json: messageData

    },
    (error, response, body) => {
        if (!error && response.statusCode == 200) {
            var recipientId = body.recipient_id;
            var messageId = body.message_id;

            if (messageId) {
                console.log("Successfully sent message with id %s to recipient %s",
                    messageId, recipientId);
            } else {
                console.log("Successfully called Send API for recipient %s",
                    recipientId);
            }
        } else {
            console.error("Failed calling Send API for userId " +
                messageData.recipient.id, response.statusCode, response.statusMessage, body.error);
        }
    }).then(callback);
}

function sendEndSeparator(recipientId, message) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "generic",
                    elements: [{
                        title: message,
                        //subtitle: aboutMessage.text,
                        //item_url: "https://gitlab.inria.fr/usnb/universal-social-network-bus",
                        image_url: "https://preview.ibb.co/jtM978/twitter_292988_1920.jpg",
                        buttons: [{
                            "title": "Main Menu",
                            "type": "postback",
                            "payload": JSON.stringify({
                                "action": "MAIN_MENU",
                                "userID": recipientId
                                })    
                        }]
                    }]
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a receipt message using the Send API.
 *
 */
function sendReceiptMessage(recipientId) {
    // Generate a random receipt ID as the API requires a unique ID
    var receiptId = "order" + Math.floor(Math.random() * 1000);

    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "receipt",
                    recipient_name: "Peter Chang",
                    order_number: receiptId,
                    currency: "USD",
                    payment_method: "Visa 1234",
                    timestamp: "1428444852",
                    elements: [{
                        title: "Oculus Rift",
                        subtitle: "Includes: headset, sensor, remote",
                        quantity: 1,
                        price: 599.00,
                        currency: "USD",
                        image_url: SERVER_URL + "/assets/riftsq.png"
                    }, {
                        title: "Samsung Gear VR",
                        subtitle: "Frost White",
                        quantity: 1,
                        price: 99.99,
                        currency: "USD",
                        image_url: SERVER_URL + "/assets/gearvrsq.png"
                    }],
                    address: {
                        street_1: "1 Hacker Way",
                        street_2: "",
                        city: "Menlo Park",
                        postal_code: "94025",
                        state: "CA",
                        country: "US"
                    },
                    summary: {
                        subtotal: 698.99,
                        shipping_cost: 20.00,
                        total_tax: 57.67,
                        total_cost: 626.66
                    },
                    adjustments: [{
                        name: "New Customer Discount",
                        amount: -50
                    }, {
                        name: "$100 Off Coupon",
                        amount: -100
                    }]
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a message with Quick Reply buttons.
 *
 */
function sendQuickReply(recipientId) {
    var usersButton = {
        "content_type": "text",
        "title": "Users",
        "payload": "USERS"
    };
    var helpButton = {
        "content_type": "text",
        "title": "Help",
        "payload": "HELP"
    };
    var aboutButton = {
        "content_type": "text",
        "title": "About",
        "payload": "ABOUT"
    };
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            text: "What do you want to do?",
            quick_replies: [
                usersButton,
                //helpButton,
                aboutButton
            ]
        }
    };

    callSendAPI(messageData);
}

/*
 * Send user list.
 *
 */
function sendUserList(recipientId, users) {
    var elements = [];
    for(var user of users) {
        var element = {
            "title": user.userId,
            "subtitle": user.about,
            "image_url": user.picture,          
            "buttons": [
                {
                    "title": "Message",
                    "type": "postback",
                    "payload": JSON.stringify({
                        "action": "TO",
                        "userID": user.userId
                    })      
                }
            ]
          }
        elements.push(element);
    }

    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            "attachment": {
                "type": "template",
                "payload": {
                  "template_type": "list",
                  "top_element_style": "compact",
                  "elements": elements,
                   "buttons": [
                    {
                      "title": "Main Menu",
                      "type": "postback",
                      "payload": JSON.stringify({
                        "action": "MAIN_MENU",
                        "userID": user.userId 
                        })            
                    }
                  ]  
                }
              }
            }
    };

    callSendAPI(messageData);
}

/*
 * Send a read receipt to indicate the message has been read
 *
 */
function sendReadReceipt(recipientId) {
    console.log("Sending a read receipt to mark message as seen");

    var messageData = {
        recipient: {
            id: recipientId
        },
        sender_action: "mark_seen"
    };

    callSendAPI(messageData);
}

/*
 * Turn typing indicator on
 *
 */
function sendTypingOn(recipientId) {
    console.log("Turning typing indicator on");

    var messageData = {
        recipient: {
            id: recipientId
        },
        sender_action: "typing_on"
    };

    callSendAPI(messageData);
}

/*
 * Turn typing indicator off
 *
 */
function sendTypingOff(recipientId) {
    console.log("Turning typing indicator off");

    var messageData = {
        recipient: {
            id: recipientId
        },
        sender_action: "typing_off"
    };

    callSendAPI(messageData);
}

/*
 * Send a message with the account linking call-to-action
 *
 */
function sendAccountLinking(recipientId) {
    console.log('sendAccountLinking');
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "button",
                    text: "Welcome. Link your account.",
                    buttons: [{
                        type: "account_link",
                        url: SERVER_URL + "/authenticate"
                    }]
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Call the Send API for users linking their accounts
 * via the checkbox plugin
 */

function callSendAPICheckbox(messageData, userApplicationId) {
    ((userApplicationId) => {
        request({
                uri: 'https://graph.facebook.com/v2.6/me/messages',
                qs: {
                    access_token: PAGE_ACCESS_TOKEN
                },
                method: 'POST',
                json: messageData

            },
            function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    var recipientId = body.recipient_id;
                    var messageId = body.message_id;

                    if (messageId) {
                        console.log("Successfully sent message with id %s to recipient %s for userApplication id %s",
                            messageId, recipientId, userApplicationId);
                        addIdentity(recipientId, userApplicationId);
                    } else {
                        console.log("Successfully called Send API for recipient %s",
                            recipientId);
                    }
                } else {
                    console.error("Failed calling Send API for userId " +
                        recipientId, response.statusCode, response.statusMessage, body.error);
                }
            });

    })(userApplicationId)
}

function addIdentity(messengerId, userApplicationId) {
    var data = {
        serviceId: "facebookmessengerbotappcivist",
        userId: userApplicationId,
        identity: messengerId,
        enabled: true
    };
    request({
            uri: 'http://localhost:3024/identities',
            method: 'POST',
            json: data
        },
        (error, response, body) => {
            if (!error && response.statusCode == 200) {
                console.log("Successfully added identity");
            } else {
                console.error("Failed calling identity API");
            }
        });
}
/*
 * Call the Send API. The message data goes in the body. If successful, we'll
 * get the message id in a response
 *
 */
function callSendAPI(messageData) {
    request({
            uri: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {
                access_token: PAGE_ACCESS_TOKEN
            },
            method: 'POST',
            json: messageData

        },
        (error, response, body) => {
            if (!error && response.statusCode == 200) {
                var recipientId = body.recipient_id;
                var messageId = body.message_id;

                if (messageId) {
                    console.log("Successfully sent message with id %s to recipient %s",
                        messageId, recipientId);
                } else {
                    console.log("Successfully called Send API for recipient %s",
                        recipientId);
                }
            } else {
                console.error("Failed calling Send API for userId " +
                    messageData.recipient.id, response.statusCode, response.statusMessage, body.error);
            }
        });
}


// Start server
// Webhooks must be available via SSL with a certificate signed by a valid
// certificate authority.
app.listen(app.get('port'), () => {
    console.log('Node app is running on port', app.get('port'));
});

module.exports = app;
module.exports.send = send;
module.exports.post = post;


/*
curl -X POST -H "Content-Type: application/json" -d '{ 
    "get_started":{
       "payload":"{action: MAIN_MENU}"
    }
 }' "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAd0hpOP0aABANG5j6NpsLsS8LJfV1uQTGWkfILlWF3pX7xu1QIzZAV6CtuWOpSJEbOdmPLykYwamqPFZCZAvtT0HKwngZBhomwxx3tOzb2DFztYPmIhAO2a3DOGvZBlYCbRyXvB7V9IUDVTFOkC7h7qrW6RiJ8QMla1YpiKvigZDZD"
 */

 /*
curl -X POST -H "Content-Type: application/json" -d '{ 
"greeting":[
    {
      "locale":"default",
      "text":"Hello!"
    }, {
      "locale":"en_US",
      "text":"Hello!"
    }
  ]
}' "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAd0hpOP0aABANG5j6NpsLsS8LJfV1uQTGWkfILlWF3pX7xu1QIzZAV6CtuWOpSJEbOdmPLykYwamqPFZCZAvtT0HKwngZBhomwxx3tOzb2DFztYPmIhAO2a3DOGvZBlYCbRyXvB7V9IUDVTFOkC7h7qrW6RiJ8QMla1YpiKvigZDZD"
*/