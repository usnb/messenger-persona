var amqp = require('amqplib/callback_api');
var Message = require('scb-node-parser/message');

amqp.connect(process.env.RABBITMQ, (err, conn) => {
    conn.createChannel((err, ch) => {
        var ex = process.env.USNB_FACEBOOK_BOT_PERSONA_EXCHANGE;

        var message = new Message({
            name: 'rafa',
            uniqueName: '1446417348757530'
        }, {
            name: 'Tester',
            uniqueName: 'Tester'
        }, 'Test Message Title', 'Test Message Body');

        ch.assertExchange(ex, 'fanout', {
            durable: true
        });
        ch.publish(ex, '', new Buffer(JSON.stringify(message)));
        console.log(" [x] Sent %s", message);
    });

    setTimeout(() {
        conn.close();
        process.exit(0)
    }, 500);
});
