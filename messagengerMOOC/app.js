
/* jshint node: true, devel: true */
'use strict';
const MAXExercise=2;
const lda = require('../llda-master/lib/lda');
var similarity = require( 'compute-cosine-similarity' );
const countpartners=2;
var users=[];
users=["Ani-Mani","Shohreh-Ahvar","Ehsan-Ahvar","Rafael-Angarita"]; 
const bitnamiurlngrok='http://68b5052e.ngrok.io';

const URLREST='http://192.168.0.110';
const
    bodyParser = require('body-parser'),
    config = require('config'),
    crypto = require('crypto'),
    express = require('express'),
    https = require('https'),
    request = require('request'),
    passport = require('passport'),
    session = require('express-session'),
        URL = require('url'),
        URLSearchParam = require('url');

require('./passport')(passport); // pass passport for configuration
const scbNodeParser = require('scb-node-parser');
var Message = require('scb-node-parser').Message;
var sender = require('./amqp-sender');
var confamqp = require('./conf/amqp-endpoint.conf');

var Message = require('scb-node-parser/message');
var HashMap = require('hashmap');

var requestPromise = require('request-promise');

var app = express();
app.set('view engine', 'ejs');
app.use(bodyParser.json({
    verify: verifyRequestSignature
}));
app.use(bodyParser.urlencoded({
    extended: false
})) // parse application/x-www-form-urlencoded
app.use(express.static('public'));
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
})); // session secret
app.use(passport.initialize());
app.use(passport.session());

/*
 * Be sure to setup your config values before running this code. You can
 * set them using environment variables or modifying the config file in /config.
 *
 */

// App Secret can be retrieved from the App Dashboard
const APP_SECRET = (process.env.MESSENGER_APP_SECRET) ?
    process.env.MESSENGER_APP_SECRET :
    config.get('appSecret');

// Arbitrary value used to validate a webhook
const VALIDATION_TOKEN = (process.env.MESSENGER_VALIDATION_TOKEN) ?
    (process.env.MESSENGER_VALIDATION_TOKEN) :
    config.get('validationToken');

// Generate a page access token for your page from the App Dashboard
const PAGE_ACCESS_TOKEN = (process.env.MESSENGER_PAGE_ACCESS_TOKEN) ?
    (process.env.MESSENGER_PAGE_ACCESS_TOKEN) :
    config.get('pageAccessToken');

// URL where the app is running (include protocol). Used to point to scripts and
// assets located at this address.
const SERVER_URL = (process.env.SERVER_URL) ?
    (process.env.SERVER_URL) :
    config.get('serverURL');

if (!(APP_SECRET && VALIDATION_TOKEN && PAGE_ACCESS_TOKEN && SERVER_URL)) {
    console.error("Missing config values");
    process.exit(1);
}


app.get('/checkbox', (req, res) => {
    res.render('sendToMessenger', {});
});

/*
 * Use your own validation token. Check that the token used in the Webhook
 * setup is the same token used here.
 *
 */
app.get('/webhook', (req, res) => {
    res.status(200).send(req.query['hub.challenge']);
    if (req.query['hub.mode'] === 'subscribe' &&
        req.query['hub.verify_token'] === VALIDATION_TOKEN) {
        console.log("Validating webhook");
        res.status(200).send(req.query['hub.challenge']);
    } else {
        console.error("Failed validation. Make sure the validation tokens match.");
        res.sendStatus(403);
    }
});




/*
 * All callbacks for Messenger are POST-ed. They will be sent to the same
 * webhook. Be sure to subscribe your app to your page to receive callbacks
 * for your page.
 * https://developers.facebook.com/docs/messenger-platform/product-overview/setup#subscribe_app
 *
 */

app.post('/webhook', (req, res) => {
    var data = req.body;

    // Make sure this is a page subscription
    if (data.object == 'page') {
        // Iterate over each entry
        // There may be multiple if batched
        data.entry.forEach((pageEntry) => {
            var pageID = pageEntry.id;
            var timeOfEvent = pageEntry.time;

            // Iterate over each messaging event
            pageEntry.messaging.forEach((messagingEvent) => {
                if (messagingEvent.optin) {
                    receivedAuthentication(messagingEvent);
                } else if (messagingEvent.message) {
                    receivedMessage(messagingEvent);
                } else if (messagingEvent.delivery) {
                    receivedDeliveryConfirmation(messagingEvent);
                } else if (messagingEvent.postback) {
                    receivedPostback(messagingEvent);
                } else if (messagingEvent.read) {
                    receivedMessageRead(messagingEvent);
                } else if (messagingEvent.account_linking) {
                    receivedAccountLink(messagingEvent);
                } else {
                    console.log("Webhook received unknown messagingEvent: ", messagingEvent);
                }
            });
        });

        // Assume all went well.
        //
        // You must send back a 200, within 20 seconds, to let us know you've
        // successfully received the callback. Otherwise, the request will time out.
        res.sendStatus(200);
    }
});

/*app.post('/linkaccount', function(req, res) {
    console.log('linking accounts...');
    var url = require('url');
    var url_parts = url.parse(request.url, true);
    var query = url_parts.query;
    console.log('linking accounts : ' + query.uname + ' and ');

    console.log(req);
    res.sendStatus(202);
});*/

/*app.post('/login', passport.authenticate('local', {
    //successRedirect: '/logged', // redirect to the secure profile section
    failureRedirect: '/authenticate', // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
}), function(req, res) {
    res.redirect('/');
});*/

/*app.post('/authenticate',
    passport.authenticate('local', {
        failureRedirect: '/authenticate',
        successRedirect: '/logged'
    })
);*/

app.post('/authenticate', (req, res, next) => {

    passport.authenticate('local', (err, user, info) => {
        if (err) {
            return next(err);
        }
        // Redirect if it fails
        if (!user) {
            return res.redirect('/authenticate');
        }
        req.logIn(user, (err) => {
            if (err) {
                return next(err);
            }
            //login success
            //get authcode and business user entity to match them later
            //when Account Linking Callback is received:
            // https://developers.facebook.com/docs/messenger-platform/webhook-reference/account-linking
            // in function receivedAccountLink(event)
            const url = new URL(req.body.redirect);
            const params = new URLSearchParams(url.search);
            console.log("authorization_code " + params.get('authorization_code'));
            app.get('cache').set(params.get('authorization_code'), user.username);
            // Redirect
            return res.redirect(req.body.redirect);
        });
    })(req, res, next);
});

/*
 * This path is used for account linking. The account linking call-to-action
 * (sendAccountLinking) is pointed to this URL.
 *
 */
app.get('/authenticate', (req, res) => {
    var accountLinkingToken = req.query.account_linking_token;
    var redirectURI = req.query.redirect_uri;

    // Authorization Code should be generated per user by the developer. This will
    // be passed to the Account Linking callback and used to match
    // the business user entity to the page-scoped ID (PSID) of the sender
    var min = 1;
    var max = Number.MAX_SAFE_INTEGER;
    const authCode = (Math.floor(Math.random() * (max - min + 1)) + min).toString();

    // Redirect users to this URI on successful login
    var redirectURISuccess = redirectURI + "&authorization_code=" + authCode;
    res.render('login', {
        accountLinkingToken: accountLinkingToken,
        redirectURI: redirectURI,
        redirectURISuccess: redirectURISuccess
    });
});

/*
 * Verify that the callback came from Facebook. Using the App Secret from
 * the App Dashboard, we can verify the signature that is sent with each
 * callback in the x-hub-signature field, located in the header.
 *
 * https://developers.facebook.com/docs/graph-api/webhooks#setup
 *
 */
function verifyRequestSignature(req, res, buf) {
    var signature = req.headers["x-hub-signature"];

    if (!signature) {
        // For testing, let's log an error. In production, you should throw an
        // error.
        console.error("Couldn't validate the signature.");
    } else {
        var elements = signature.split('=');
        var method = elements[0];
        var signatureHash = elements[1];

        var expectedHash = crypto.createHmac('sha1', APP_SECRET)
            .update(buf)
            .digest('hex');

        if (signatureHash != expectedHash) {
            throw new Error("Couldn't validate the request signature.");
        }
    }
}

/*
 * Authorization Event
 *
 * The value for 'optin.ref' is defined in the entry point. For the "Send to
 * Messenger" plugin, it is the 'data-ref' field. Read more at
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/authentication
 *
 */
function receivedAuthentication(event) {
    console.log("received authentication: " + JSON.stringify(event));
    if (event.optin != 'undefined') {
        console.log("sending message using user ref: " + event.optin.user_ref +
            " for logged user " + event.optin.ref);

        var messageData = {
            recipient: {
                user_ref: event.optin.user_ref
            },
            message: {
                text: "Welcome!"
            }
        };

        callSendAPICheckbox(messageData, event.optin.ref);
    } else {

        var senderID = event.sender.id;
        var recipientID = event.recipient.id;
        var timeOfAuth = event.timestamp;

        // The 'ref' field is set in the 'Send to Messenger' plugin, in the 'data-ref'
        // The developer can set this to an arbitrary value to associate the
        // authentication callback with the 'Send to Messenger' click event. This is
        // a way to do account linking when the user clicks the 'Send to Messenger'
        // plugin.
        var passThroughParam = event.optin.ref;

        console.log("Received authentication for user %d and page %d with pass " +
            "through param '%s' at %d", senderID, recipientID, passThroughParam,
            timeOfAuth);

        // When an authentication is received, we'll send a message back to the sender
        // to let them know it was successful.
        sendTextMessage(senderID, "Authentication successful");
    }
}

/*
 * Message Event
 *
 * This event is called when a message is sent to your page. The 'message'
 * object format can vary depending on the kind of message that was received.
 * Read more at https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-received
 *
 * For this example, we're going to echo any text that we get. If we get some
 * special keywords ('button', 'generic', 'receipt'), then we'll send back
 * examples of those bubbles to illustrate the special message bubbles we've
 * created. If we receive a message with an attachment (image, video, audio),
 * then we'll simply confirm that we've received the attachment.
 *
 */
function receivedMessage(event) {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;  
    var timeOfMessage = event.timestamp;
    var message = event.message;

    console.log("Received message for user %d and page %d at %d with message:",
        senderID, recipientID, timeOfMessage);
    console.log(JSON.stringify(message));

    var isEcho = message.is_echo;
    var messageId = message.mid;
    var appId = message.app_id;
    var metadata = message.metadata;

    // You may get a text or attachment but not both
    var messageText = message.text;
    var messageAttachments = message.attachments;
    var quickReply = message.quick_reply;

    if (isEcho) {
        // Just logging message echoes to console
        console.log("Received echo for message %s and app %d with metadata %s",
            messageId, appId, metadata);
        return;
    } else if (quickReply) {

        var quickReplyPayload = quickReply.payload;
        console.log("Quick reply for message %s with payload %s",
            messageId, quickReplyPayload);
     //   console.log("Quick replyquickReply"+quickReply);
        switch (quickReplyPayload) {
            case 'USERS':
                //sendTextMessage(senderID, 'to whom?');
                var message = new Message();
                message._from = {
                    name:  event.sender.name,
                    uniqueName: senderID
                };
                message._message = "ls";
                message._type = "COMMAND";
                message._persona = confamqp.exchange.name;
                sender.post(message);
                break;
            case 'MOOC': // ahvar (3) : Comes here when people click on MOOC , is this case in teh right place ? 
		console.log('Check MOOC');
                //sendTextMessage(senderID, 'to whom?');
		// take enrolled courses 
                var message = new Message();

		request.get(URLREST+'/api/enrollment/v1/enrollment?', {
		  'auth': {
		    'bearer': '790cfdb132807bf82a3f719f770fc5dd4e8c5ca5'
		  }
		}, function (error, response, body) {

		//console.log('inside rest API');
		//console.log(URLREST+'/api/enrollment/v1/enrollment?');
		body=JSON.parse(body);


		//console.log(recipientID);
		sendCourseList(senderID, body, "Enrolled courses, select a course"); // ahvar (4) : not sure if recipientID is correct item to send or not 

		});
                break;
            case 'partners': // ahvar (10)

		    var options = [];
		    var photos = [":checkered_flag: ", ":heart: ", ":bridge_at_night: ", " :girl: ", " :frog: ", ":ghost: "] ;// to be cheked 
		    console.log('senderID'+senderID);
		//  CourseUsers.set('1978765938881465', 'course-v1:edX+DemoX+Demo_Course' ); // it was quickReply.user.name
		    var SelectedCourse=CourseUsers.get(senderID); // it was quickReply.user.name before 18 dec 
   
		    while((SelectedCourse.localeCompare(SelectedCourse.replace('+','%2B')))!=0)
		    {
		    SelectedCourse=SelectedCourse.replace('+','%2B');

		    }

		      var message=[];

		    recom('Ani-Mani', SelectedCourse,users).then(function(message){
			console.log("message.length"+message.length);
		    for (var i = 0; i < message.length; i++) {
			var option = {
			 // "picture": photos[i%(photos.length)], //to be checked 
			  "picture": "https://preview.ibb.co/mBxSaT/handprint_472090_1280.jpg", //to be checked 
			  "about": "Nothing special",
			  "userId":message[i]
			}

			options.push(option);

		      }// end of for
		//	options.push(option);  // extra 18 dec 
		console.log(options[0]);
		console.log(options[1]);
 // it is extra. jusst because list with 1 item does not work

		   sendUserList(senderID,options ); // ahvar : not sure if recipientID is correct item to send or not 
		    });// end of recom // IMPORTANT : all the functions in this break should be inside recom func


		//ahvar : send  a list with this options with the type that we use for userlist  
                
                break;
	    case 'exercise':
		        var SelectedCourse=CourseUsers.get(senderID); // it was quickReply.user.name before 18 dec 
			var coursename=CourseIDname.get(SelectedCourse);
			var text= "You are in course " +  coursename + ". Select an exercise. "; // to check 

			while((SelectedCourse.localeCompare(SelectedCourse.replace('+','%2B')))!=0)
			{
			SelectedCourse=SelectedCourse.replace('+','%2B');

			}


			var link=URLREST+"/api/courses/v1/blocks/?username=sho2&&depth=all&requested_fields=student_view_multi_device&block_counts=problem&student_view_data=problem&block_types_filter=problem&course_id="+SelectedCourse;
			//console.log(link);
			request.get(link,{
			//course-v1:edX+DemoX+Demo_Course
			//username=sho2&&depth=all&requested_fields=student_view_multi_device&block_counts=problem&student_view_data=problem&block_types_filter=problem&course_id="+SelectedCourse,{
			// 27 Nov to do : username should come from inputs in the APIs
			//display_name
			//student_view_url
			  'auth': {
			    'bearer': '790cfdb132807bf82a3f719f770fc5dd4e8c5ca5'
			  }
			}, function (error, response, body) {

			//console.log(body);
			/////20 Nov
			body=JSON.parse(body);
			//console.log(Object.keys(body.blocks).length);
			//console.log(body.blocks[Object.keys(body.blocks)[0]].student_view_url);
			var options = [];
			var temp;

			for (var i = 0    ; i < Object.keys(body.blocks).length && i<MAXExercise; i++) {
			temp=body.blocks[Object.keys(body.blocks)[i]].student_view_url;
			temp=temp.replace(URLREST,bitnamiurlngrok);
			if(body.blocks[Object.keys(body.blocks)[i]].display_name!="")
			{


				var option = {
					title: body.blocks[Object.keys(body.blocks)[i]].display_name,
					type: "web_url",
				        url: temp,


				}
			}
			else
			{
			var option = {

					type: "web_url",
				        url: temp,
					title: 'Exercise '+(i+1),

			    }


			}

			options.push(option);
			      console.log('i'+i);
		      }


			      sendGeneralButtonMessage(senderID, options, text);


			}); //end of exersise API




			break;

		case "forum":


		        var SelectedCourse=CourseUsers.get(senderID); // it was quickReply.user.name before 18 dec 
			var coursename=CourseIDname.get(SelectedCourse);
			var text= "You are in course " +  coursename + ". Select a thread. "; // to check 

			while((SelectedCourse.localeCompare(SelectedCourse.replace('+','%2B')))!=0)
			{
			SelectedCourse=SelectedCourse.replace('+','%2B');

			}

			var threadlink=URLREST+"/api/discussion/v1/threads/?course_id="+SelectedCourse;
			//console.log(link);
			request.get(threadlink,{
			  'auth': {
			    'bearer': '790cfdb132807bf82a3f719f770fc5dd4e8c5ca5'
			  }
			}, function (error, response, body) {


			body=JSON.parse(body);
			//console.log(body.results);
			//console.log(body.results[0].title);
			      var threadsoptions = [];

			for (var i = 0; i < body.results.length && i<MAXExercise; i++) {

			if(body.results[i].title!="")
			{
				var option = {
					"name": body.results[i].title,

				}
				ThreadtitleID.set(body.results[i].title,body.results[i].id);  // to use it later in the case //to do 27 NOV
			}
				threadsoptions.push(option);
			}


			//body.results[i].title
			//threadsoption extraction 27 nov



			/////20 Nov
			      
			     sendList(senderID,threadsoptions,text, "forum_list");

			});//end of thread API





			break;

            default:
                var message = new Message();
                message._from = {
                    name:  event.sender.name,
                    uniqueName: senderID
                };
                message._message = quickReplyPayload.toLowerCase();
                message._type = "COMMAND";
                message._persona = confamqp.exchange.name;
                sender.post(message);
            }
        return;
    }
    console.log('User state: ' + userState.get(senderID));
    console.log('UMessage: ' + messageText.toLowerCase());
    if(userState.get(senderID) != undefined) {
        if(messageText.toLowerCase() === "quit") {
            sendEndSeparator(senderID, 'You ended the conversation with ' + userState.get(senderID) + '.');
            userState.delete(senderID);
        } else {
            //sendTextMessage(senderID, 'Message sent to ' + userState.get(senderID) + '!');
            var message = new Message();
            message._from = {
                name:  event.sender.name,
                uniqueName: senderID
            };
            message._to = [
                userState.get(senderID)];
            message._message = messageText;
            message._type = 'TO';
            message._persona = confamqp.exchange.name;
            sender.post(message);
        }
    } else if (messageText) {

        // If we receive a text message, check to see if it matches any special
        // keywords and send back the corresponding example. Otherwise, just echo
        // the text we received.
        switch (messageText) {
            case 'image':
                sendImageMessage(senderID);
                break;

            case 'gif':
                sendGifMessage(senderID);
                break;

            case 'audio':
                sendAudioMessage(senderID);
                break;

            case 'video':
                sendVideoMessage(senderID);
                break;

            case 'file':
                sendFileMessage(senderID);
                break;

            case 'button':
                sendButtonMessage(senderID);
                break;

            case 'generic':
                sendGenericMessage(senderID);
                break;

            case 'receipt':
                sendReceiptMessage(senderID);
                break;

            case 'quick reply':
                sendQuickReply(senderID);
                break;
            case 'add menu':
                console.log('add menu');
                break 
            case 'read receipt':
                sendReadReceipt(senderID);
                break;

            case 'typing on':
                sendTypingOn(senderID);
                break;

            case 'typing off':
                sendTypingOff(senderID);
                break;

            case 'account linking':
                sendAccountLinking(senderID);
                break;

            default:
                /*var parsedMessage = scbNodeParser.getMessage(messageText);
                console.log('parsedMessage: ' + parsedMessage.getMessage());
                parsedMessage._from = {
                    name:  event.sender.name,
                    uniqueName: senderID
                };
                parsedMessage._persona = confamqp.exchange.name;
                sender.post(parsedMessage);*/
                console.log('receivedMessage.default');
                sendQuickReply(senderID);
        }
    } else if (messageAttachments) {
        sendTextMessage(senderID, "Message with attachment received");
    }
}

/** 
 * Messages from socialbus 
 **/
function post(msg) {

    switch (msg._type) {
        case 'LS':
            sendUserList(msg._to.uniqueName, msg._message);
            break;
        case 'TO':
            console.log('Message from social bus ' + userState.get(msg._to.uniqueName));
            if(userState.get(msg._to.uniqueName) === undefined) {
                userState.set(msg._to.uniqueName, msg._from.name);
                console.log('userState set for ' + msg._to.uniqueName + ' set to ' + msg._from.uniqueName);

                var uname = msg._to.uniqueName;
                var umessage = msg._message;
                sendStartSeparator(msg._to.uniqueName, "This is a conversation with " +  
                msg._from.name + ". Write \"quit\" to end it.", 
                    function(uname, umessage)  {
                        sendTextMessage(msg._to.uniqueName, msg._message);
                    }
                );      
                
            } else {

                sendTextMessage(msg._to.uniqueName, msg._message);
            }
            break;
        case 'ABOUT':
            sendGenericMessage(msg._to.uniqueName, msg._message);
            break;
        case 'INFO':
            sendTextMessage(msg._to.uniqueName, msg._message);
            break;
        default:

    }
    /*var messageText = '';
    if(msg._from.uniqueName === 'socialbus')
        messageText = msg.getMessage();
    else
        messageText = 'From: ' + msg.getFrom().name + '. ' + msg.getSubject() + '\n Message: \n' + msg.getMessage();
    */
    
}


/*
 * Delivery Confirmation Event
 *
 * This event is sent to confirm the delivery of a message. Read more about
 * these fields at https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-delivered
 *
 */
function receivedDeliveryConfirmation(event) {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;
    var delivery = event.delivery;
    var messageIDs = delivery.mids;
    var watermark = delivery.watermark;
    var sequenceNumber = delivery.seq;

    if (messageIDs) {
        messageIDs.forEach((messageID) => {
            console.log("Received delivery confirmation for message ID: %s",
                messageID);
        });
    }

    console.log("All message before %d were delivered.", watermark);
}


/*
 * Postback Event
 *
 * This event is called when a postback is tapped on a Structured Message.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/postback-received
 *
 */
var userState = new HashMap();
//ahvar
var ThreadUsers=new HashMap();
var CourseUsers=new HashMap();
var DocUsers=new HashMap();
var CommentUsers=new HashMap();
var QUsers=new HashMap();
var CourseIDname=new HashMap();
var ThreadtitleID=new HashMap();
// ahvar : to do : add set for above hashs

function receivedPostback(event) {
    console.log('log');
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;
    var timeOfPostback = event.timestamp;

    // The 'payload' param is a developer-defined field which is set in a postback
    // button for Structured Messages.
    var payload = event.postback.payload;

    console.log("Received postback for user %d and page %d with payload '%s' " +
        "at %d", senderID, recipientID, payload, timeOfPostback);

    // When a postback is called, we'll send a message back to the sender to
    // let them know it was successful
    var jPayload = JSON.parse(payload);
    //sendTextMessage(senderID, "Postback called from action " + JSON.stringify(jPayload.action));
    switch(jPayload.action) {
        case 'TO':

            userState.set(senderID, jPayload.userID);

            sendStartSeparator(senderID, "This is a conversation with " +  
            jPayload.userID + ". Write \"quit\" to end it.");
            break;
        case 'MAIN_MENU':
            sendQuickReply(senderID);
            break;
        case 'FROMMOOC': //ahvar (6) 
	    console.log("he selected a course");

	    
            CourseUsers.set(senderID, CourseIDname.get(jPayload.Coursename));   //  with ID
	    console.log("selected COurse fo senderID "+senderID+" is "+CourseUsers.get(senderID));

            sendQuickReplyMOOC(senderID,jPayload.Coursename); // with name
 //send menu including partner study         
	    break;
        case 'forum_list': //ahvar (6) 
	    // send menu with main-menu, answer, question     
	    break;
        default:
    }
}

/*
 * Message Read Event
 *
 * This event is called when a previously-sent message has been read.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-read
 *
 */
function receivedMessageRead(event) {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;

    // All messages before watermark (a timestamp) or sequence have been seen.
    var watermark = event.read.watermark;
    var sequenceNumber = event.read.seq;

    console.log("Received message read event for watermark %d and sequence " +
        "number %d", watermark, sequenceNumber);
}

/*
 * Account Link Event
 *
 * This event is called when the Link Account or UnLink Account action has been
 * tapped.
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/account-linking
 *
 */
function receivedAccountLink(event) {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;

    var status = event.account_linking.status;
    var authCode = event.account_linking.authorization_code;

    const userApplicationId = app.get('cache').get(authCode);
    console.log("Received account link event with for user %d with status %s " +
        "and auth code %s for userApplicationId %s", senderID, status, authCode, userApplicationId);

    //delete the key since we'll not need it anymore
    app.get('cache').del(authCode);
    addIdentity(senderID, userApplicationId);
    sendTextMessage(senderID,
        "Done! You've been linked with username \"" + userApplicationId + "\"");
}

/*
 * Send an image using the Send API.
 *
 */
function sendImageMessage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "image",
                payload: {
                    url: SERVER_URL + "/assets/rift.png"
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a Gif using the Send API.
 *
 */
function sendGifMessage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "image",
                payload: {
                    url: SERVER_URL + "/assets/instagram_logo.gif"
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send audio using the Send API.
 *
 */
function sendAudioMessage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "audio",
                payload: {
                    url: SERVER_URL + "/assets/sample.mp3"
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a video using the Send API.
 *
 */
function sendVideoMessage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "video",
                payload: {
                    url: SERVER_URL + "/assets/allofus480.mov"
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a file using the Send API.
 *
 */
function sendFileMessage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "file",
                payload: {
                    url: SERVER_URL + "/assets/test.txt"
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a text message using the Send API.
 *
 */
function sendTextMessage(recipientId, messageText) {
    console.log('recipientId : ' + recipientId);
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            text: messageText,
            metadata: "DEVELOPER_DEFINED_METADATA"
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a text message using the Send API.
 *
 */
function send(message) {
    console.log('recipientName : ' + message._to.name + ' recipientId : ' + message._to.uniqueName);
    var messageData = {
        recipient: {
            id: message._to.uniqueName
        },
        message: {
            text: message._message,
            metadata: "DEVELOPER_DEFINED_METADATA"
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a button message using the Send API.
 *
 */
function sendButtonMessage(recipientId) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "button",
                    text: "This is test text",
                    buttons: [{
                        type: "web_url",
                        url: "https://www.oculus.com/en-us/rift/",
                        title: "Open Web URL"
                    }, {
                        type: "postback",
                        title: "Trigger Postback",
                        payload: "DEVELOPER_DEFINED_PAYLOAD"
                    }, {
                        type: "phone_number",
                        title: "Call Phone Number",
                        payload: "+16505551234"
                    }]
                }
            }
        }
    };

    callSendAPI(messageData);
}


/*
 * Send Genetal button message using the Send API and buttons as an input.
 *
 */
function sendGeneralButtonMessage(recipientId,button, textvalue) {
   button.push({
                            "title": "Main Menu",
                            "type": "postback",
                            "payload": JSON.stringify({
                                "action": "MAIN_MENU",
                                "userID": recipientId
                                })    
                        });
   var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "button",
                    text: textvalue,
                    buttons:button
                }
            }
        }
    };

    callSendAPI(messageData);
}
/*
 * Send a Structured Message (Generic Message type) using the Send API.
 *
 */
function sendGenericMessage(recipientId, aboutMessage) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "generic",
                    elements: [{
                        title: "About me",
                        subtitle: aboutMessage.text,
                        item_url: "https://gitlab.inria.fr/usnb/universal-social-network-bus",
                        image_url: aboutMessage.picture,
                        buttons: [{
                            "title": "Main Menu",
                            "type": "postback",
                            "payload": JSON.stringify({
                                "action": "MAIN_MENU",
                                "userID": recipientId
                                })    
                        }, {
                            "type":"web_url",
                            "url": aboutMessage.url,
                            "title": "Visit Page",
                            "webview_height_ratio": "full"
                        }],
                    }]
                }
            }
        }
    };

    callSendAPI(messageData);
}

function sendStartSeparator(recipientId, message, callback) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "generic",
                    elements: [{
                        title: message,
                        //subtitle: aboutMessage.text,
                        //item_url: "https://gitlab.inria.fr/usnb/universal-social-network-bus",
                        image_url: "https://preview.ibb.co/jtM978/twitter_292988_1920.jpg",
                        /*buttons: [{
                            "title": "Main Menu",
                            "type": "postback",
                            "payload": JSON.stringify({
                                "action": "MAIN_MENU",
                                "userID": recipientId
                                })    
                        }]*/
                    }]
                }
            }
        }
    };

    requestPromise({
        uri: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {
            access_token: PAGE_ACCESS_TOKEN
        },
        method: 'POST',
        json: messageData

    },
    (error, response, body) => {
        if (!error && response.statusCode == 200) {
            var recipientId = body.recipient_id;
            var messageId = body.message_id;

            if (messageId) {
                console.log("Successfully sent message with id %s to recipient %s",
                    messageId, recipientId);
            } else {
                console.log("Successfully called Send API for recipient %s",
                    recipientId);
            }
        } else {
            console.error("Failed calling Send API for userId " +
                messageData.recipient.id, response.statusCode, response.statusMessage, body.error);
        }
    }).then(callback);
}

function sendEndSeparator(recipientId, message) {
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "generic",
                    elements: [{
                        title: message,
                        //subtitle: aboutMessage.text,
                        //item_url: "https://gitlab.inria.fr/usnb/universal-social-network-bus",
                        image_url: "https://preview.ibb.co/jtM978/twitter_292988_1920.jpg",
                        buttons: [{
                            "title": "Main Menu",
                            "type": "postback",
                            "payload": JSON.stringify({
                                "action": "MAIN_MENU",
                                "userID": recipientId
                                })    
                        }]
                    }]
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a receipt message using the Send API.
 *
 */
function sendReceiptMessage(recipientId) {
    // Generate a random receipt ID as the API requires a unique ID
    var receiptId = "order" + Math.floor(Math.random() * 1000);

    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "receipt",
                    recipient_name: "Peter Chang",
                    order_number: receiptId,
                    currency: "USD",
                    payment_method: "Visa 1234",
                    timestamp: "1428444852",
                    elements: [{
                        title: "Oculus Rift",
                        subtitle: "Includes: headset, sensor, remote",
                        quantity: 1,
                        price: 599.00,
                        currency: "USD",
                        image_url: SERVER_URL + "/assets/riftsq.png"
                    }, {
                        title: "Samsung Gear VR",
                        subtitle: "Frost White",
                        quantity: 1,
                        price: 99.99,
                        currency: "USD",
                        image_url: SERVER_URL + "/assets/gearvrsq.png"
                    }],
                    address: {
                        street_1: "1 Hacker Way",
                        street_2: "",
                        city: "Menlo Park",
                        postal_code: "94025",
                        state: "CA",
                        country: "US"
                    },
                    summary: {
                        subtotal: 698.99,
                        shipping_cost: 20.00,
                        total_tax: 57.67,
                        total_cost: 626.66
                    },
                    adjustments: [{
                        name: "New Customer Discount",
                        amount: -50
                    }, {
                        name: "$100 Off Coupon",
                        amount: -100
                    }]
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a message with Quick Reply buttons.
 *
 */
function sendQuickReply(recipientId) {
    var usersButton = {
        "content_type": "text",
        "title": "Users",
        "payload": "USERS"
    };
    var moocButton = {   // ahvar (1)
        "content_type": "text",
        "title": "MOOC",
        "payload": "MOOC"
    };
    var helpButton = {
        "content_type": "text",
        "title": "Help",
        "payload": "HELP"
    };
    var aboutButton = {
        "content_type": "text",
        "title": "About",
        "payload": "ABOUT"
    };
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            text: "What do you want to do?",
            quick_replies: [
                usersButton,
                //helpButton,
                aboutButton,
		moocButton  // ahvar (2)
            ]
        }
    };

    callSendAPI(messageData);
}

/*
 * Send a message with Quick Reply buttons for MOOC. //ahvar (7)
 *
 */
function sendQuickReplyMOOC(recipientId,Coursename) {
    var forumButton = {
        "content_type": "text",
        "title": "Forum",
        "payload": "forum"
    };
    var partnersButton = {   // ahvar
        "content_type": "text",
        "title": "Study Partners",
        "payload": "partners"
    };
    var helpButton = {
        "content_type": "text",
        "title": "Help",
        "payload": "HELP"
    };
    var MainmenuButton = { // ahvar (8) not sure what should we write here for main menu
        "content_type": "text",
        "title": "Mainmenu",
        "payload": "Mainmenu"
    };
    var exerciseButton = {
        "content_type": "text",
        "title": "Exercises",
        "payload": "exercise"
    };
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            text: "What do you want to do? You are in course " +  Coursename + ". Write \"quit\" to end it.",
            quick_replies: [
                partnersButton,
                //helpButton,
          //      MainmenuButton, //ahvar (9):  comment (8) to be checked and added 
		forumButton,
		exerciseButton
            ]
        }
    };

    callSendAPI(messageData);
}


/*
 * Send user list.
 *
 */
function sendUserList(recipientId, users) {
console.log("last ");
console.log("users "+users);
    var elements = [];
    for(var user of users) {
console.log("user "+user);
        var element = {
            "title": user.userId,
            "subtitle": user.about,
            "image_url": user.picture,          
            "buttons": [
                {
                    "title": "Message",
                    "type": "postback",
                    "payload": JSON.stringify({
                        "action": "TO",
                        "userID": user.userId
                    })      
                }
            ]
          }
        elements.push(element);
    }
  //    elements.push(element); // extra 18 dec ahvar

    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            "attachment": {
                "type": "template",
                "payload": {
                  "template_type": "list",
                  "top_element_style": "compact",
                  "elements": elements,
                   "buttons": [
                    {
                      "title": "Main Menu",
                      "type": "postback",
                      "payload": JSON.stringify({
                        "action": "MAIN_MENU",
                   //     "userID": user.userId 
                        "userID": recipientId 
                        })            
                    }
                  ]  
                }
              }
            }
    };

    callSendAPI(messageData);
}


/*
 * Send general list.
 *
 */



function sendList(recipientId, items,text, actionname) { //ahvar (5) not sure the action it should be 'TO'. we defined new action called FROMMOOC  ? 


    var elements = [];
    for(var t=0; t< items.length;t++) {

        var element = {
            "title": items[t].name,

//            "subtitle": "ii hh",  
  //          "image_url": 'https://preview.ibb.co/kF6W28/shoe_print_3482282_1280.jpg',
            "buttons": [
                {
                    "title": "Select", // ahvar 18 dec, it was message we changed it to Select
                    "type": "postback",
                   "payload": JSON.stringify({
                       "action": actionname,
                        "name": items[t].name,
                    })      
                }
            ]
          }//element 

         //   CourseIDname.set(courses[t].course_details.course_name,courses[t].course_details.course_id);   



        elements.push(element);
    }//for

// just because fb does not show a list with 1 item 
//element.title=element.title+"2"
  //      elements.push(element);
console.log(elements);

   var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            "attachment": {
                "type": "template",
                "payload": {
                  "template_type": "list",
                  "top_element_style": "compact",
                  "elements": elements,
                   "buttons": [
                    {
                      "title": "Main Menu",
                      "type": "postback",
                      "payload": JSON.stringify({
                        "action": "MAIN_MENU",
                        "userID": recipientId
                        })            
                    }
                  ]  
                }
              }
            }
    };

    callSendAPI(messageData);
}


/*
 * Send course list.
 *
 */



function sendCourseList(recipientId, courses, textmsg) { //ahvar (5) not sure the action it should be 'TO'. we defined new action called FROMMOOC  ? 


    var elements = [];
    for(var t=0; t< courses.length;t++) {

        var element = {
            "title": courses[t].course_details.course_name,

//            "subtitle": "ii hh",  
  //          "image_url": 'https://preview.ibb.co/kF6W28/shoe_print_3482282_1280.jpg',
            "buttons": [
                {
                    "title": "Select", // ahvar 18 dec, it was message we changed it to Select
                    "type": "postback",
                   "payload": JSON.stringify({
                       "action": "FROMMOOC",
                        "Coursename": courses[t].course_details.course_name,
                    })      
                }
            ]
          }//element 

console.log('coursename'+courses[t].course_details.course_name);
console.log('courseID'+courses[t].course_details.course_id);
            CourseIDname.set(courses[t].course_details.course_name,courses[t].course_details.course_id);   



        elements.push(element);
    }//for

// just because fb does not show a list with 1 item 
//element.title=element.title+"2"
  //      elements.push(element);
console.log(elements);

   var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
	    //"text": textmsg,
            "attachment": {
                "type": "template",
                "payload": {
   //               "text": "These are your enrolled courses",  // here we should add somehow this, with this line it
                  "template_type": "list",
                  "top_element_style": "compact",
                  "elements": elements,
                   "buttons": [
                    {
                      "title": "Main Menu",
                      "type": "postback",
                      "payload": JSON.stringify({
                        "action": "MAIN_MENU",
                        "userID": recipientId
                        })            
                    }
                  ]  
                }
              }
            }
    };

    callSendAPI(messageData);
}

/*
 * Send a read receipt to indicate the message has been read
 *
 */

function sendReadReceipt(recipientId) {
    console.log("Sending a read receipt to mark message as seen");

    var messageData = {
        recipient: {
            id: recipientId
        },
        sender_action: "mark_seen"
    };

    callSendAPI(messageData);
}

/*
 * Turn typing indicator on
 *
 */
function sendTypingOn(recipientId) {
    console.log("Turning typing indicator on");

    var messageData = {
        recipient: {
            id: recipientId
        },
        sender_action: "typing_on"
    };

    callSendAPI(messageData);
}

/*
 * Turn typing indicator off
 *
 */
function sendTypingOff(recipientId) {
    console.log("Turning typing indicator off");

    var messageData = {
        recipient: {
            id: recipientId
        },
        sender_action: "typing_off"
    };

    callSendAPI(messageData);
}

/*
 * Send a message with the account linking call-to-action
 *
 */
function sendAccountLinking(recipientId) {
    console.log('sendAccountLinking');
    var messageData = {
        recipient: {
            id: recipientId
        },
        message: {
            attachment: {
                type: "template",
                payload: {
                    template_type: "button",
                    text: "Welcome. Link your account.",
                    buttons: [{
                        type: "account_link",
                        url: SERVER_URL + "/authenticate"
                    }]
                }
            }
        }
    };

    callSendAPI(messageData);
}

/*
 * Call the Send API for users linking their accounts
 * via the checkbox plugin
 */

function callSendAPICheckbox(messageData, userApplicationId) {
    ((userApplicationId) => {
        request({
                uri: 'https://graph.facebook.com/v2.6/me/messages',
                qs: {
                    access_token: PAGE_ACCESS_TOKEN
                },
                method: 'POST',
                json: messageData

            },
            function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    var recipientId = body.recipient_id;
                    var messageId = body.message_id;

                    if (messageId) {
                        console.log("Successfully sent message with id %s to recipient %s for userApplication id %s",
                            messageId, recipientId, userApplicationId);
                        addIdentity(recipientId, userApplicationId);
                    } else {
                        console.log("Successfully called Send API for recipient %s",
                            recipientId);
                    }
                } else {
                    console.error("Failed calling Send API for userId " +
                        recipientId, response.statusCode, response.statusMessage, body.error);
                }
            });

    })(userApplicationId)
}

function addIdentity(messengerId, userApplicationId) {
    var data = {
        serviceId: "facebookmessengerbotappcivist",
        userId: userApplicationId,
        identity: messengerId,
        enabled: true
    };
    request({
            uri: 'http://localhost:3024/identities',
            method: 'POST',
            json: data
        },
        (error, response, body) => {
            if (!error && response.statusCode == 200) {
                console.log("Successfully added identity");
            } else {
                console.error("Failed calling identity API");
            }
        });
}
/*
 * Call the Send API. The message data goes in the body. If successful, we'll
 * get the message id in a response
 *
 */
function callSendAPI(messageData) {
    request({
            uri: 'https://graph.facebook.com/v2.6/me/messages',
            qs: {
                access_token: PAGE_ACCESS_TOKEN
            },
            method: 'POST',
            json: messageData

        },
        (error, response, body) => {
            if (!error && response.statusCode == 200) {
                var recipientId = body.recipient_id;
                var messageId = body.message_id;

                if (messageId) {
                    console.log("Successfully sent message with id %s to recipient %s",
                        messageId, recipientId);
                } else {
                    console.log("Successfully called Send API for recipient %s",
                        recipientId);
                }
            } else {
                console.error("Failed calling Send API for userId " +
                    messageData.recipient.id, response.statusCode, response.statusMessage, body.error);
            }
        });
}


// Start server
// Webhooks must be available via SSL with a certificate signed by a valid
// certificate authority.
app.listen(2345, () => {
    console.log('Node app is running on port', app.get('port'));
});

module.exports = app;
module.exports.send = send;
module.exports.post = post;


/*
curl -X POST -H "Content-Type: application/json" -d '{ 
    "get_started":{
       "payload":"{action: MAIN_MENU}"
    }
 }' "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAd0hpOP0aABANG5j6NpsLsS8LJfV1uQTGWkfILlWF3pX7xu1QIzZAV6CtuWOpSJEbOdmPLykYwamqPFZCZAvtT0HKwngZBhomwxx3tOzb2DFztYPmIhAO2a3DOGvZBlYCbRyXvB7V9IUDVTFOkC7h7qrW6RiJ8QMla1YpiKvigZDZD"
 */

 /*
curl -X POST -H "Content-Type: application/json" -d '{ 
"greeting":[
    {
      "locale":"default",
      "text":"Hello!"
    }, {
      "locale":"en_US",
      "text":"Hello!"
    }
  ]
}' "https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAd0hpOP0aABANG5j6NpsLsS8LJfV1uQTGWkfILlWF3pX7xu1QIzZAV6CtuWOpSJEbOdmPLykYwamqPFZCZAvtT0HKwngZBhomwxx3tOzb2DFztYPmIhAO2a3DOGvZBlYCbRyXvB7V9IUDVTFOkC7h7qrW6RiJ8QMla1YpiKvigZDZD"
*/




//// ahvar : recommendation section


const recom = async (name, course,users) => {
//async function main() {

var collection=[];

var API=URLREST+"/api/discussion/v1/threads/?course_id="+course;  // 5 dec to check, should I give the name here ? 
console.log(API);
var threadIDs=[];
var threadendores=[];



/////////////here request calling for questions ///////
  let body = await doRequest(API);
console.log("to make sure 2");
//console.log(body);
                                  var array=[];
                                                      body=JSON.parse(body);
                                                 //     console.log(body.results[0]);
                            
                                if(body.results!=null)
                            for(var i=0; i<body.results.length;i++){ //for1
                                array=[];
                            threadIDs[i]=body.results[i].id;
                            if(body.results[i].type=="question")
                                    threadendores[i]=body.results[i].has_endorsed;
                            else
                                    threadendores[i]=-1;
                                console.log("thread "+i+"user: "+body.results[i].author+" withtext : "+body.results[i].raw_body+" users: "+DocUsers.get(body.results[i].author));
                            if(DocUsers.get(body.results[i].author)==null){ // if there is not already in the hash for this user any question recorded 
                                console.log("not saved ");
                                array.push(body.results[i].raw_body);}
                                else{
                            //    console.log(DocUsers.get(body.results[i].author));
                                array=DocUsers.get(body.results[i].author);
                                array.push(body.results[i].raw_body);
                            }
                            DocUsers.set(body.results[i].author,array);
                             
                        //         console.log('DocQuestion for this user'+DocUsers.get(body.results[i].author));

                          //  console.log('thread '+i+ ':  ' + threadIDs[i]);

                            //console.log( threadIDs[i]);
                            } //for1

                             console.log('   ***********        \n');
                       //10 dec     });  // 






                             console.log('   *****comments******        \n');

                                       //            for(var l=0; l<users.length;l++)
                                                   for(var k=0; k<threadIDs.length;k++){//for2


                                    if(threadendores[k]!=-1)
                                    var API2=URLREST+"/api/discussion/v1/comments/?course_id="+course+"&endorsed=0&thread_id=";
                                    else
                                    var API2=URLREST+"/api/discussion/v1/comments/?course_id="+course+"&thread_id=";
        
                                API2=API2+threadIDs[k];
                                //To do : if type is discussion we should not give endors to it 
                             //       console.log(API2);

  let body = await doRequest(API2);
//console.log(res);

                                                    //console.log( " thread: "+threadIDs[k]);
                                                    //console.log( body);
                                                    //console.log('\n \n thread '+k+ ':  ' + body);
                                                             var array=[];

                                                    body=JSON.parse(body);
                                                    if(body!=null )
                                                    if(body.results!=null )
                                                    {

                                                                               for(var i=0; i<body.results.length;i++){ //for results.length
                                                        array=[];

                                                        console.log("thread "+k+"user: "+body.results[i].author+" withtext : "+body.results[i].raw_body+" dousers: "+DocUsers.get(body.results[i].author));
                                                        if(DocUsers.get(body.results[i].author)==null){ // if there is not already in the hash for this user any question recorded 
                                                            console.log("not saved ");
                                                            array.push(body.results[i].raw_body);}
                                                        else{

                                                            array=DocUsers.get(body.results[i].author);
                                                            array.push(body.results[i].raw_body);
                                                        }
                                                        DocUsers.set(body.results[i].author,array);
                                                  //          console.log(DocUsers.get(body.results[i].author));
                                                       }//for results.length
                                


                                                    }

                             //       });//end req


                                }//for 2   
/// taking the data from Docusers(user) to collection 
//console.log('DocUsers after comment '+DocUsers);

for (var y=0;y<users.length; y++)
{
//collection[y]=[];
if(DocUsers.get(users[y])!=null)
{

    console.log('array : '+DocUsers.get(users[y]));
 //   collection.push(DocUsers.get(users[y]));
    collection.push(DocUsers.get(users[y]));
console.log('after Q phase, Collection for user '+users[y]+' is : '+ collection[y]);
 //   CommentUsers.set(users[y],(DocUsers.get(users[y])).length-(QUsers.get(users[y]).length));
}


}
console.log(collection);

var users2=[];
users2=users;
var u=users.length;
/// remove the users that has no text in the forum 
for (var t=0,  y=0;y<u; y++)
{
if(DocUsers.get(users[y])!=null)
{	//users.splice(y, 1);

	users[t]=users2[y];
t++;
 //       collection.splice(y, 1);
}

}


console.log(users);


/// LDA+cosine for recommending study partners
var counterm=0;
var terms=new HashMap();
var probabilities = [];
var partnersimilarity = [];
var partnerID ; // should be array 
var Tresults = [];
var dense = [];
var Totalresult=[];

collection.forEach((documents, i) => {
  const results = lda(documents, 1, 2, null, null, null, 123);  // we will take 1 topic including several terms, the next lines are based on this fact 
console.log('\n Topic of Document '+(i+1));
console.log(results);
//Tresults[i].push(results);
  // Save the probabilities for each group. The values should be the same, since we're using the same random seed.

  results.forEach(group => {
    group.forEach(row => {
      if(terms.get(row.term)==null)
      {
	terms.set(row.term,counterm);
	counterm++;
      }
    });
  });

      Totalresult.push(results);

});
   console.log('\n ******************LDA runs finished ***************** \n');
   console.log('\n ******************dense vector creation  ***************** \n');
var o=0;


Totalresult.forEach(results => {
    dense[o]=[]; // we initialize the 
    for(var b=0;b<counterm;b++)
	dense[o][b]=0;
  console.log(dense[o]);
  results.forEach(group => {
    

    group.forEach(row => {


	//      console.log('term is '+row.term+' and the value is '+ terms.get(row.term));
	      if(terms.get(row.term)!=null)
	      {
	
		dense[o][terms.get(row.term)]=row.probability;

	       }
		
	
	

    });

  });
o++;
});


//3 dec To do , just do for the user 
var s;
var partnersindex=[];
var partners=[];


for(var l=0;l<countpartners;l++)
for ( var i = 0; i < dense.length; i++ ) {
     partnersimilarity[l]=-1;
     for ( var j = 0; j < dense.length; j++ ) {

         if(i!=j && users[i]==name && partnersindex.indexOf(j)==-1)
            {
		console.log('dense[i]'+dense[i]);
		console.log('dense[j]'+dense[j]);
            s = similarity( dense[i], dense[j] );
            console.log('Similarity for' + name + 'and '+ users[j]+'is  '+s); //To do : we should retrive the nam eof student number i 
          if(partnersimilarity[l]<s)  // 
       //   if(partnersimilarity[i]-s<2)  // here we can say if 
          {
          partnersimilarity[l]=s;
      //  partnerID=(j)
; //  
	  partnersindex[l]=j;
	  
          }
         }

             


      }


// 10 dec 5:20 PM here we can merge QUsers and CommentUsers and similarity 

        if (users[i]==name)
{
        partners.push(users[partnersindex[l]]);
            console.log('Recommended study partner for'+name+'is student '+users[partnersindex[l]]); //To do : we should retrive the nam eof student number i 
}
   }




console.log(partners);

// partners=await sendback(partners);
return partners;









/// 3 dec : to DO: put chats in the collection array 

//take chat of all students 



}






/////////////////////3 dec



function doRequest(url) {
console.log("to make sure 0");
  return new Promise(function (resolve, reject) {
    request.get(url,{

          'auth': {
            'bearer': '6d5d5ac879788378bdb19930ce80c36d8db8e9f3'
          },

                }, function (error, res,body) {
//console.log('error of request '+error);
//console.log('body of req'+body);
console.log("to make sure 1");
      if (!error && res.statusCode == 200) {
        resolve(body);
      } else {
        reject(error);
      }
    });
  });
}


